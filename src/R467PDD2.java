/* Rajkin Hossain */

import java.util.*;
import java.io.*;

public class R467PDD2 {
	
	static FastInput input;
	//public static Scanner input;
	public static FastOutput output;
	
	private static Graph graph;
	private static int nodes, edges;
	 
	public static void main(String [] args) throws IOException {
		input = new FastInput(System.in);
		//input = new Scanner(new File(System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "input.txt"));
		output = new FastOutput();
		while(input.hasNext()){
			nodes = input.nextInt();
			edges = input.nextInt();
			
			graph = new Graph(nodes);
			addEdgesFromInput();
			graph.source = input.nextInt() - 1;
			graph.bfs(graph.source, 1);
			
			if (graph.winPossible) {
				output.println("Win");
				pathPrint();
			}
			else if (graph.drawPossible) {
				output.println("Draw");
			} else {
				output.println("Lose");
			}
			
		}
		output.flush();
	}
	
	private static void pathPrint() {
		output.print(graph.path.get(graph.path.size() - 1) + 1);
		for(int i = graph.path.size() - 2; i >= 0; i--) {
			output.print(" "+(graph.path.get(i) + 1));
		}
	}
	
	private static void addEdgesFromInput() {
		for(int i = 0; i < nodes; i++) {
			int nextNodes = input.nextInt();
			for (int counter = 0; counter < nextNodes; counter++) {
				int nextNode = input.nextInt();
				graph.addEdgeSingleDirectional(i, nextNode - 1);
			}
		}
	}
	
	
	/* MARK: Graph domain class */
	
    public static class Graph {
        public int nodes;
        public int source;
        public ArrayList<Integer> [] neighbors;
        public int [][] parent;
        public ArrayList<Integer> path;
        public ArrayDeque<Pair> queue;
        public boolean [][] marked;
        public boolean [] visited;
        public boolean winPossible, drawPossible;
         
        @SuppressWarnings("unchecked")
		public Graph(int nodes) {
            this.nodes = nodes;
            source = -1;
            neighbors = new ArrayList[nodes];
            path = new ArrayList<Integer>();
            queue = new ArrayDeque<Pair>();
            marked = new boolean[nodes][2];
            parent = new int[nodes][2];
            visited = new boolean[nodes];
            winPossible = false;
            drawPossible = false;
            for(int i = 0; i<nodes; i++){
            		neighbors[i] = new ArrayList<>();
            		parent[i][0] = -1;
            		parent[i][1] = -1;
            }
        }
        
        public void addEdgeSingleDirectional(int u, int v) {
        		neighbors[u].add(v);
        }
        
        public void addEdgeBiDirectional(int u, int v) {
    			neighbors[u].add(v);
    			neighbors[v].add(u);
        }
        
        public int swap(int a) {
        		return a == 0 ? 1 : 0;
        }
        
        public void getPath(int node, int isOdd) {
        		parent[source][1] = -1;
        		while (node != -1) {
        			path.add(node);
        			node = parent[node][isOdd];
        			isOdd = swap(isOdd);
        		}
        }
        
        public void bfs(int currentNode, int isOdd) {
        		queue.add(new Pair(currentNode, isOdd));
        		while(!queue.isEmpty()) {
        			currentNode = queue.peek().node;
        			isOdd = queue.poll().isOdd;
            		if (win(currentNode, isOdd)) {
            			winPossible = true;
        				getPath(currentNode, isOdd);
            			break;
            		}
            		isOdd = swap(isOdd);
            		for (int i = 0; i < neighbors[currentNode].size(); i++) {
            			int neighborNode = neighbors[currentNode].get(i);
            			if (!marked[neighborNode][isOdd]) {
            				marked[neighborNode][isOdd] = true;
            				parent[neighborNode][isOdd] = currentNode;
            				queue.add(new Pair(neighborNode, isOdd));
            			}
            		}
        		}
        		if (!winPossible) {
        			hasDirectedCycle(source);
        		}
        }
        
        public boolean win(int node, int isOdd) {
        		return (neighbors[node].size() == 0 && isOdd == 0) ? true : false;
        }
        
        /*
        public boolean hasDirectedCycle(int s){
        	
	    		if(visited[s]==1) return true;
	    		if(visited[s]==2) return false;
	    	
	    		visited[s]=1;
	    		for(int i=0;i<neighbors[s].size();i++){
	    			if(hasDirectedCycle(neighbors[s].get(i))) return true;
	    		}
	    		visited[s]=2;
	    	
	    		return false;
        }*/
        
        public void hasDirectedCycle(int u) {
        		if (!drawPossible) {
		    		for (int i = 0; i < neighbors[u].size(); i++) {
		    			int neighborNode = neighbors[u].get(i);
		    			if (!visited[neighborNode]) {
		    				visited[neighborNode] = true;
		    				hasDirectedCycle(neighborNode);
		    				visited[neighborNode] = false;
		    			} else {
		    				drawPossible = true;
		    				break;
		    			}
		    		}
        		}
        }
    }
    
    
    /* MARK: Pair class */
    
    public static class Pair {
    		public int node, isOdd;
    		public Pair(int node, int isOdd) {
    			this.node = node;
    			this.isOdd = isOdd;
    		}
    }
    
    
	/* MARK: FastInput and FastOutput */

	public static class FastInput {
	    BufferedReader reader;
	    StringTokenizer tokenizer;

	    public FastInput(InputStream stream){
	        reader = new BufferedReader(new InputStreamReader(stream));
	    }

	    public String next() {
	        return nextToken();
	    }

	    public boolean hasNext(){
	    	try {
				return reader.ready();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
	    }

	    public String nextToken() {
	        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
	            String line = null;
	            try {
	                line = reader.readLine();
	            } catch (IOException e) {
	                throw new RuntimeException(e);
	            }
	            if (line == null) {
	                return null;
	            }
	            tokenizer = new StringTokenizer(line);
	        }
	        return tokenizer.nextToken();
	    }

	    public int nextInt() {
	        return Integer.parseInt(nextToken());
	    }
	    
	    public long nextLong() {
	        return Long.valueOf(nextToken());
	    }
	}

    public static class FastOutput extends PrintWriter {
    		FastOutput() { super(new BufferedOutputStream(System.out)); }
    }
}