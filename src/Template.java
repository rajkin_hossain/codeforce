/* Rajkin Hossain */

import java.util.*;
import java.io.*;

public class Template {
	
	public static void main(String [] args) throws IOException{
		input = new FastInput(System.in);
		//Scanner input = new Scanner(new File(System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "input.txt"));
		output = new FastOutput();
		while(input.hasNext()){
			
		}
		output.flush();
	}
	
	
	/* MARK: Graph domain class */
	
    public static class Graph {
        public int nodes;
        public ArrayList<Integer> neighbors[];
         
        @SuppressWarnings("unchecked")
		public Graph(int nodes) {
            this.nodes = nodes;
            neighbors = new ArrayList[nodes];
            for(int i = 0; i<nodes; i++){
            		neighbors[i] = new ArrayList<>();
            }
        }
        
        public void addEdgeSingleDirectional(int u, int v) {
        		neighbors[u].add(v);
        }
        
        public void addEdgeBiDirectional(int u, int v) {
    			neighbors[u].add(v);
    			neighbors[v].add(u);
        }
    }
    
    
    /* MARK: FastInput and FastOutput */
	
	public static FastInput input;
	public static FastOutput output;

	public static class FastInput {
	    BufferedReader reader;
	    StringTokenizer tokenizer;

	    public FastInput(InputStream stream){
	        reader = new BufferedReader(new InputStreamReader(stream));
	    }

	    public String next() {
	        return nextToken();
	    }

	    public boolean hasNext(){
	    	try {
				return reader.ready();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
	    }

	    public String nextToken() {
	        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
	            String line = null;
	            try {
	                line = reader.readLine();
	            } catch (IOException e) {
	                throw new RuntimeException(e);
	            }
	            if (line == null) {
	                return null;
	            }
	            tokenizer = new StringTokenizer(line);
	        }
	        return tokenizer.nextToken();
	    }

	    public int nextInt() {
	        return Integer.parseInt(nextToken());
	    }
	    
	    public long nextLong() {
	        return Long.valueOf(nextToken());
	    }
	}

    public static class FastOutput extends PrintWriter {
    		FastOutput() { super(new BufferedOutputStream(System.out)); }
    }
}
